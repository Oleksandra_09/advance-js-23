
const url = "https://api.ipify.org/?format=json";
const btn = document.querySelector(".btn");
const body = document.querySelector("body");
const getIp = async () => {
    try {
        
        const myIp = await fetch(url).then(data => data.json());
        const { ip } = myIp;
        const url2 = `http://ip-api.com/json/${ip}?fields=continent,country,region,regionName,city`;
        const ip2 = await fetch(url2).then(data => data.json())
        const {continent,country,region,city,regionName} = ip2
        body.insertAdjacentHTML("beforeEnd",
        `<p>Континент: ${continent}</p>
        <p>Країна: ${country}</p>
        <p>Регіон: ${region}</p>
        <p>Місто: ${city}</p>
        <p>Район: ${regionName}</p>
    `)

    } catch (error) {
        console.log(error)
    }
}
btn.addEventListener("click" , () => getIp())
