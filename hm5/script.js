
const spinner = document.querySelector(".custom-loader");
class Card {
    constructor() {
        this.mainContainer = document.createElement(`div`); //головний контейнер
        this.divContent = document.createElement(`div`); // обгортка для всіх елементів

    }

    createElements() {
        this.divContent.classList.add(`card-post`);
        this.mainContainer.classList.add(`card`);
        this.mainContainer.append(this.divContent)
    }

    render(place = `body`) {
        this.createElements();
        document.querySelector(place).append(this.mainContainer);
    }
}

class ArticleCard extends Card {
    constructor(name, username, email, title, text, id) {
        super();
        this.name = name;
        this.username = username;
        this.email = email;
        this.titleText = title;
        this.postText = text;
        this.id = id;

        this.titleElem = document.createElement('h3');
        this.postElem = document.createElement("p");
        this.emailEl = document.createElement("h2");
        this.usernameEl = document.createElement("h2");
        this.nameEl = document.createElement("h2");

        this.btnAdd = document.createElement(`button`);
        this.btnDelete = document.createElement(`button`);


    }

    cardDelete() {
        fetch(`https://ajax.test-danit.com/api/json/posts/${this.id}`, {
            method: 'DELETE',
        })
            .then(response => {
                if (response.ok) {
                    this.divContent.remove()
                }
            })
            .catch(err =>
                console.log(err)
            )
    }




    createElements() {
        super.createElements();

        this.nameEl.innerText = this.name;
        this.usernameEl.innerText = this.username;
        this.emailEl.innerText = this.email;
        this.titleElem.innerText = this.titleText;
        this.postElem.innerText = this.postText;

        this.divContent.append(this.nameEl);
        this.nameEl.append(this.usernameEl);
        this.nameEl.append(this.emailEl);
        this.divContent.append(this.titleElem);
        this.divContent.append(this.postElem);
        this.divContent.append(this.btnAdd);
        this.divContent.append(this.btnDelete);

        this.btnAdd.classList.add(`card-post__card-btn`);
        this.btnAdd.innerText = "додати"

        this.btnDelete.classList.add(`card-post__card-btn`);
        this.btnDelete.innerText = "видалити";

        this.titleElem.classList.add(`card-post__title`);
        this.postElem.classList.add(`card-post__title-text`);
        this.emailEl.classList.add(`card-post__user-email`);
        this.usernameEl.classList.add(`card-post__user-name`);
        this.nameEl.classList.add(`card-post__user`);



        this.btnDelete.addEventListener('click', () => this.cardDelete())

    }


}


const func = async () => {
    try {
        const users = await fetch("https://ajax.test-danit.com/api/json/users").then(data => data.json());
        const posts = await fetch("https://ajax.test-danit.com/api/json/posts").then(data => data.json());
        spinner.style.display = "none"
        posts.forEach(post => {
            const { name, username, email } = users.find(user => user.id === post.userId);
            const { title, body: text, id } = post;
            new ArticleCard(name, username, email, title, text, id).render();
           
        })
    } catch (error) {
      console.log(error)
    }



}
func();
