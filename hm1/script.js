

/*1.Поясніть своїми словами, як ви розумієте, як працює прототипне наслідування в Javascript?
Прототипне наслідування дозволяє одному об'єкту базуватися на іншому. Саме тому об'єкти розділяють між собою спільні властивості.
*/
/*2.Для чого потрібно викликати super() у конструкторі класу-нащадка?
В конструкторі класу-нащадка ми пишемо super() , щоб виконати його батьківський (базовий) конструктор, інакше об'єкт для this не буде створено.
*/

class Employee {
    constructor(name, age, salary) {
        this._name = name;
        this._age = age;
        this._salary = salary;
    }

    get name() {
        return console.log(`${this._name}`); 
    }
    set name(value) {
        this._name = value;
    }
    get age() {
        return console.log(`${ this._age}`);
    }
    set age(value) {
        this._age = value;

    }
    get salary() {
        return console.log(`${this._salary}`);
    }
    set salary(value) {
        this._salary = value;
    }
}

const employee = new Employee("Alexa", 28, 4500);
console.log(employee);
employee.name;
employee.age;
employee.salary;

class Programmer extends Employee {
    constructor(salary, lang) {
        super(salary);
        this._salary = salary;
        this._lang = lang;

    }
    get lang() {
        return  this._lang;
    }
    set lang(value) {
        this._lang = value;
    }
    get salary() {
        return this._salary*3;
    }
    set salary(value) {
        this._salary = value;
    }
}

const programmer = new Programmer(+"4500","eng");
const programmer1 = new Programmer(+"500","ukr");
const programmer2 = new Programmer(+"600", "pol");
const programmer3 = new Programmer(+"120", "fran");


console.log(programmer.lang, programmer.salary);
console.log(programmer2.lang, programmer2.salary);
console.log(programmer3.lang, programmer3.salary)
