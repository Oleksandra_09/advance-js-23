//Наведіть кілька прикладів, коли доречно використовувати в коді конструкцію try...catch
/**
 * Коли ми припускаємо,що в якій частині може бути помилка ,і щоб уникнути зупинки виконання кода в цілому використуваємо конструкцію try...catch.
 *  Наприклад:
 * 
 * Виведіть цей масив на екран у вигляді списку (тег ul – список має бути згенерований за допомогою Javascript).
 * 
На сторінці повинен знаходитись div з id="root", куди і потрібно буде додати цей список (схоже завдання виконувалось в модулі basic).
Перед додаванням об'єкта на сторінку потрібно перевірити його на коректність (в об'єкті повинні міститися всі три властивості - author, name, price). Якщо якоїсь із цих властивостей немає, в консолі має висвітитися помилка із зазначенням - якої властивості немає в об'єкті.
Ті елементи масиву, які не є коректними за умовами попереднього пункту, не повинні з'явитися на сторінці.
 * 
 */

const books = [
  { 
    author: "Люсі Фолі",
    name: "Список запрошених",
    price: 70 
  }, 
  {
   author: "Сюзанна Кларк",
   name: "Джонатан Стрейндж і м-р Норрелл",
  }, 
  { 
    name: "Дизайн. Книга для недизайнерів.",
    price: 70
  }, 
  { 
    author: "Алан Мур",
    name: "Неономікон",
    price: 70
  }, 
  {
   author: "Террі Пратчетт",
   name: "Рухомі картинки",
   price: 40
  },
  {
   author: "Анґус Гайленд",
   name: "Коти в мистецтві",
  }
];

const root = document.getElementById('root');
const list = document.createElement('ul');
root.append(list);

function createItemsOfList(array) {
  array.forEach(elem => {
      try {
          if (elem.author && elem.name && elem.price) {
              const item = document.createElement('li');
              item.innerText = `Author: ${elem.author} , Name: ${elem.name} , Price: ${elem.price}`;
              list.append(item);
          }
          if (!elem.author) {
              throw new SyntaxError("Неполные данные: отсутствует поле author");
          }
          if (!elem.name) {
              throw new SyntaxError("Неполные данные: отсутствует поле  name");
          }
          if (!elem.price) {
              throw new SyntaxError("Неполные данные: отсутствует поле  price");
          }

      } catch (e) {
          console.log(e.message);
      }
  })
}

createItemsOfList(books);

