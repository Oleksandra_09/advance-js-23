
/**
 * Теоретичне питання
Поясніть своїми словами, що таке AJAX і чим він корисний при розробці Javascript.
AJAX- ідея,яка дозволяє отримувати і передавати інформацію без перезавантаження сторінки.

Завдання
Отримати список фільмів серії Зоряні війни та вивести на екран список персонажів для кожного з них.

Технічні вимоги:
Надіслати AJAX запит на адресу https://ajax.test-danit.com/api/swapi/films та отримати список усіх фільмів серії Зоряні війни
Для кожного фільму отримати з сервера список персонажів, які були показані у цьому фільмі. Список персонажів можна отримати з властивості characters.
Як тільки з сервера буде отримана інформація про фільми, відразу вивести список усіх фільмів на екрані. Необхідно вказати номер епізоду, назву фільму, а також короткий зміст (поля episodeId, name, openingCrawl).
Як тільки з сервера буде отримано інформацію про персонажів будь-якого фільму, вивести цю інформацію на екран під назвою фільму.
Необов'язкове завдання підвищеної складності
Поки завантажуються персонажі фільму, прокручувати під назвою фільму анімацію завантаження. Анімацію можна використовувати будь-яку. Бажано знайти варіант на чистому CSS без використання JavaScript.
 */

const filmContainer = document.querySelector(".filmContainer");
const spinner = document.querySelector(".custom-loader");
fetch(`https://ajax.test-danit.com/api/swapi/films`)
    .then(response => response.json())
    .then(films => {
        films.map(filmObj => {
            filmContainer.insertAdjacentHTML("beforeend",
                `<li>Назва фільму : ${filmObj.name} <p> Номер єпізоду: ${filmObj.episodeId}</p><p>короткий опис :${filmObj.openingCrawl}</p></li>`);

            filmObj.characters.map(URLFilm => {
                spinner.style.display = "none"
                fetch(URLFilm)
                    .then(person => person.json())
                    .then(person => filmContainer.insertAdjacentHTML("beforeend",
                            `<li>ім'я персонажа : ${person.name}
                              рік народження : ${person.birthYear}
                               стать : ${person.gender}
                               </li>`))
                              
            }
            )
           
        })

    }
    )
    .catch(error => {
        throw new error(`my error!!!!`)
    })
