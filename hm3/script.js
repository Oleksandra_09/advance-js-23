
/**
 * Поясніть своїми словами, як ви розумієте, що таке деструктуризація і навіщо вона потрібна.
 * Деструктуризація нам потріба для того,щоб поділити складні структури коду на більш прості. В основному це або об'єкти,або масиви.
 */

//Завдання 1
//Дві компанії вирішили об'єднатись, і для цього їм потрібно об'єднати базу даних своїх клієнтів.

//У вас є 2 масиви рядків, у кожному з них – прізвища клієнтів. Створіть на їх основі один масив, який буде об'єднання двох масивів без повторюваних прізвищ клієнтів.

const clients1 = ["Гилберт", "Сальваторе", "Пирс", "Соммерс", "Форбс", "Донован", "Беннет"];
const clients2 = ["Пирс", "Зальцман", "Сальваторе", "Майклсон"];

const clientsTogther = [...clients1,...clients2];

const filterClientsTogther = clientsTogther.filter((number, index, numbers) => {
return numbers.indexOf(number) === index;
});
console.log(filterClientsTogther);
//Завдання 2

const characters = [
    {
        name: "Елена",
        lastName: "Гилберт",
        age: 17,
        gender: "woman",
        status: "human"
    },
    {
        name: "Кэролайн",
        lastName: "Форбс",
        age: 17,
        gender: "woman",
        status: "human"
    },
    {
        name: "Аларик",
        lastName: "Зальцман",
        age: 31,
        gender: "man",
        status: "human"
    },
    {
        name: "Дэймон",
        lastName: "Сальваторе",
        age: 156,
        gender: "man",
        status: "vampire"
    },
    {
        name: "Ребекка",
        lastName: "Майклсон",
        age: 1089,
        gender: "woman",
        status: "vempire"
    },
    {
        name: "Клаус",
        lastName: "Майклсон",
        age: 1093,
        gender: "man",
        status: "vampire"
    }
];

function createNew (){
    charactersShortInfo = [];
    characters.forEach(person => {
        let {name, lastName, age} = person;
        charactersShortInfo.push( {name, lastName, age});
    })
    return charactersShortInfo;
}

console.log(createNew(characters));


//завдання 3 

//У нас є об'єкт' user:

const user1 = {
  name: "John",
  years: 30
};
const {name, years, isAdmin = false} = user1;
const body =  document.querySelector(`body`);
const div = document.createElement(`div`);
body.append(div);
div.innerText = `ім'я : ${name} , вік : ${years}, адмін: ${isAdmin} `;



//завдання 4
const satoshi2020 = {
    name: 'Nick',
    surname: 'Sabo',
    age: 51,
    country: 'Japan',
    birth: '1979-08-21',
    location: {
      lat: 38.869422, 
      lng: 139.876632
    }
  }
  
  const satoshi2019 = {
    name: 'Dorian',
    surname: 'Nakamoto',
    age: 44,
    hidden: true,
    country: 'USA',
    wallet: '1A1zP1eP5QGefi2DMPTfTL5SLmv7DivfNa',
    browser: 'Chrome'
  }
  
  const satoshi2018 = {
    name: 'Satoshi',
    surname: 'Nakamoto', 
    technology: 'Bitcoin',
    country: 'Japan',
    browser: 'Tor',
    birth: '1975-04-05'
  }

const fullProfile = {...satoshi2018,...satoshi2019,...satoshi2020};
console.log(fullProfile);

//Завдання 5
//Дано масив книг. Вам потрібно додати до нього ще одну книгу, не змінюючи існуючий масив (в результаті операції має бути створено новий масив).

const books = [{
  name: 'Harry Potter',
  author: 'J.K. Rowling'
}, {
  name: 'Lord of the rings',
  author: 'J.R.R. Tolkien'
}, {
  name: 'The witcher',
  author: 'Andrzej Sapkowski'
}];

const bookToAdd = {
  name: 'Game of thrones',
  author: 'George R. R. Martin'
}
const newBooks = [...books, bookToAdd]
console.log(newBooks);

//Завдання 6
//Даний об'єкт employee. Додайте до нього властивості age і salary, не змінюючи початковий об'єкт (має бути створено новий об'єкт, який включатиме всі необхідні властивості). Виведіть новий об'єкт у консоль.

const employee = {
  name: 'Vitalii',
  surname: 'Klichko'
}
const newEmployee = {...employee, age:`56`, salary: `4120` };
console.log(newEmployee);

//Завдання 7

//Доповніть код так, щоб він коректно працював

const array = ['value', () => 'showValue'];

// Допишіть код тут
let[value,showValue] = array;
alert(value); // має бути виведено 'value'
alert(showValue());  // має бути виведено 'showValue'
